package com.magicbytes.kotlin.inspections;

import com.intellij.codeInspection.InspectionToolProvider;
import com.magicbytes.kotlin.inspections.ExampleInspection;
import org.jetbrains.annotations.NotNull;

public class ExampleProvider implements InspectionToolProvider {
    @NotNull
    @Override
    public Class[] getInspectionClasses() {
        return new Class[]{ExampleInspection.class};
    }
}
