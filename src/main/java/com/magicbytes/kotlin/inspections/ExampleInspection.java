package com.magicbytes.kotlin.inspections;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlin.idea.inspections.AbstractKotlinInspection;

public class ExampleInspection extends AbstractKotlinInspection {
    @Nls
    @NotNull
    @Override
    public String getDisplayName() {
        return "Kotlin Test";
    }
}
